# OpenLDAP Debian packages FORK

This is a fork, from https://github.com/ltb-project/openldap-deb
Here to build arm64 packages using GitLab CI.

# OpenLDAP Debian packages

Files needed to build LDAP Tool Box OpenLDAP Debian packages

## Documentation

See http://ltb-project.org/wiki/documentation/openldap-deb

## Download

See http://ltb-project.org/wiki/download#packages_for_debianubuntu
